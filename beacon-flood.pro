TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
LIBS += -lpcap
SOURCES += \
        gbeaconhdr.cpp \
        gcommon.cpp \
        gdot11hdr.cpp \
        gmac.cpp \
        gradiotaphdr.cpp \
        gtrace.cpp \
        main.cpp

HEADERS += \
    gbeaconhdr.h \
    gcommon.h \
    gdot11hdr.h \
    gmac.h \
    gradiotaphdr.h \
    gtrace.h
